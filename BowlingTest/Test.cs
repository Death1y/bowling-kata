﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void CreateGame()
        {
            game = new Game();
        }

        public void RollMany(int Trys, int pinDown)
        {
            for (int i = 1; i < Trys; i++)
            {
                game.roll(pinDown);
            }
        }

        [Test()]
        public void GutterGame()
        {
            RollMany(20, 0);

            Assert.That(game.Score, Is.EqualTo(0));
        }

        [Test()]
        public void onepinfall()
        {
            RollMany(20, 1);

            Assert.That(game.Score, Is.EqualTo(20));
        }

        [Test()]
        public void OnespareGame()
        {
            game.roll(1);
            game.roll(9);
            RollMany(18, 0);

            Assert.That(game.Score, Is.EqualTo(10));
        }

        [Test()]
        public void OneStrikeGame()
        {
            game.roll(10);
            RollMany(18, 0);

            Assert.That(game.Score, Is.EqualTo(10));
        }

        [Test()]
        public void AllStrikeGame()
        {
            RollMany(12, 10);

            Assert.That(game.Score, Is.EqualTo(300));
        }

        [Test()]
        public void AllSpareGame()
        {
            RollMany(21, 5);

            Assert.That(game.Score, Is.EqualTo(145));
        }
    }
}
